tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer i = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> assign(p1={$e1.st},p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(n={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {i++;} -> conditional_statement(e1={$e1.st},e2={$e2.st},e3={$e3.st}, j={i.toString()})
    ;
    